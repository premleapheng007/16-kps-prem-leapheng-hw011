import React from "react";
import { ListGroup } from "react-bootstrap";

function ClaculateHistory(props) {
  var array2 = props.myResult;
  var list = array2.map((e) => (
    <ListGroup.Item key={e.id}>{e.myVal}</ListGroup.Item>
  ));
  return (
    <div>
      <h1>Calculate History</h1>
      <ListGroup>{list}</ListGroup>
    </div>
  );
}

export default ClaculateHistory;
