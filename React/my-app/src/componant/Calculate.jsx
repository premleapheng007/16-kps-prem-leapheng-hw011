import React, { Component } from "react";
import "bootstrap/dist/css/bootstrap.min.css";
import { Container, Row, Card, Button, Form, Modal } from "react-bootstrap";
import "./calStyle.css";
import CalculateHistory from "./CalculateHistory.jsx";

let my_array = [];
let i = 1;
class Calculate extends Component {
  constructor() {
    //use state
    super();
    this.state = {
      number1: "",
      number2: "",
      array: [],
    };
  }

  handleChange = (event) => {
    this.setState({
      [event.target.name]: event.target.value,
    });
  };

  show = () => {
    this.setState({
      show: true,
    });
  };
  handleClose = () => {
    this.setState({
      show: false,
    });
  };
  AlertFun = () => {
    return (
      <>
        {/* <Modal show={this.state.show} onHide={this.handleClose}>
          <Modal.Header closeButton>
            <Modal.Title>Modal heading</Modal.Title>
          </Modal.Header>
          <Modal.Body>Woohoo, you're reading this text in a modal!</Modal.Body>
          <Modal.Footer>
            <Button variant="secondary" onClick={this.handleClose}>
              Close
            </Button>
            <Button variant="primary" onClick={this.handleClose}>
              Save Changes
            </Button>
          </Modal.Footer>
        </Modal> */}
      </>
    );
  };
  Calculate = (e) => {
    var a = parseFloat(this.state.number1);
    var b = parseFloat(this.state.number2);
    var numfirst=document.getElementById("first");
    var numSecond=document.getElementById("second")
    var result;
    var op = this.option.value;

    if (this.state.number1 === "") {
      this.show();
      return;
    }
    if (isNaN(this.state.number1)) {
      numfirst.style.border ="1px solid red";
      numfirst.value="";
      numfirst.placeholder="Invalid first Input ";
      return
    }
    if (!isNaN(this.state.number1)) {
      numfirst.style.border ="1px";
    }
    if (this.state.number2 === "") {
      this.show();
      return;
    }
    if (isNaN(this.state.number2)) {
      numSecond.style.border ="1px solid red";
      numSecond.value="";
      numSecond.placeholder="Invalid second Input ";
      return
    }
    if (!isNaN(this.state.number2)) {
      numSecond.style.border ="1px";
    }

    if (op === "1") {
      result = a + b;
    } else if (op === "2") {
      result = a - b;
    } else if (op === "3") {
      result = a * b;
    } else if (op === "4") {
      result = a / b;
    } else if (op === "5") {
      result = a % b;
    }

    var obj = {
      id: i++,
      myVal: result,
    };

    my_array.push(obj);
    // console.log(my_array)
    // console.log()
    // this.setState({array:this.state.array.concat(result)});
    // alert(result);
    this.setState({
      my_array: [...this.state.array, result], // Spreading operator
    });
  };
  render() {
    return (
      <div>
        <>
          <Modal show={this.state.show} onHide={this.handleClose}>
            <Modal.Header closeButton>
              <Modal.Title>Invalid Iput</Modal.Title>
            </Modal.Header>
            <Modal.Body>Please input Some value</Modal.Body>
            <Modal.Footer>
              <Button variant="secondary" onClick={this.handleClose}>
                Close
              </Button>
            </Modal.Footer>
          </Modal>
        </>
        <Container>
          <Row className="mt-5">
            <div className="col-xl-6">
              <Card style={{ width: "500px" }} className="bg-secondary">
                <Card.Img
                  className="img"
                  variant="top"
                  src="https://purepng.com/public/uploads/large/purepng.com-calculator-icon-android-lollipopsymbolsiconsgooglegoogle-iconsandroid-lollipoplollipop-iconsandroid-50-721522597141natii.png"
                />
                <Card.Body>
                  <input
                    id="first"
                    className="form-control"
                    type="text"
                    name="number1"
                    value={this.state.number1}
                    onChange={this.handleChange}
                    placeholder="Input First Value"
                    // ref={(num1) => (this.num1 = num1)}
                  ></input>
                  <input
                    id="second"
                    className="form-control"
                    type="text"
                    name="number2"
                    value={this.state.number2}
                    onChange={this.handleChange}
                    placeholder="Input Seconds Value"
                    // ref={(num2) => (this.num2 = num2)}
                  ></input>
                  <Form.Control
                    as="select"
                    className="m-1 option"
                    custom
                    ref={(option) => (this.option = option)}
                  >
                    <option value="1">+ Plus</option>
                    <option value="2">- Subtract</option>
                    <option value="3">* Multiple</option>
                    <option value="4">/ Divide</option>
                    <option value="5">% Modulus</option>
                  </Form.Control>
                  <Button
                    className="m-1 button"
                    onClick={(e) => this.Calculate(e)}
                  >
                    Calculate
                  </Button>
                </Card.Body>
              </Card>
            </div>
            <div className="col-xl-6 top ">
              <CalculateHistory myResult={my_array} />
            </div>
          </Row>
        </Container>
      </div>
    );
  }
}

export default Calculate;
